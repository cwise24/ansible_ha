# ansible_ha

Ansible playbook(s) designed to turn up VE's for High Availability (HA).

Tested with:
- TMOS 15.1.9.1, 17.1.0.2
- Ansible 2.10.7

Restart TMM for [issue](https://cdn.f5.com/product/bugtracker/ID882609.html)

### Ansible Only
For pure Ansible deployment, make sure *ha_lab.yml* has this task *initial_ansible.yml* set correctly:

```
tasks:

    - name: intial do tasks
      include_tasks: initial_ansible.yml
      loop: "{{ groups['adc'] }}"
```

To edit individual host options
 - Self IP
 - VLAN
 - Route Domain
 - Interfaces
 - ConfigSync
 - Unicast Failover

You can edit the individual host files located in *host_vars/* directory

### Declarative Onboard
You may also use the DO variation

In this deployment, Ansible will use the *uri* module to authenticate and get a token.

**PREREQUISITE**
>You have to have the Declarative Onboarding RPM installed and can obtained from [here](https://github.com/F5Networks/f5-declarative-onboarding/releases). 

```
tasks:

    - name: intial do tasks
      include_tasks: initial_do.yml
      loop: "{{ groups['adc'] }}"
```
To edit individual host options
 - Self IP
 - VLAN
 - Route Domain
 - Interfaces
 - ConfigSync
 - Unicast Failover

You can edit the individual host JSON files located in *DO/* directory.

## Getting started

Disable password rules:
```
tmsh modify /auth password-policy policy-enforcement disabled
```
Set root/default admin/admin:
```
tmsh modify auth password root

tmsh modify auth password admin
```

VMWare workstation settings to get sync to work
```
tmsh modify sys db tm.tcplargereceiveoffload value disable
tmsh modify sys db tm.tcpsegmentationoffload value disable
```
Set extramb:
``` 
tmsh modify sys db restjavad.useextramb value true
tmsh modify sys db provision.extramb value 2048
bigstart restart restjavad
```
## Running

To run the playbook:
You can call inventory from a directory

```
ansible-playbook -i inventory ha_lab.yml
```
## Other Useful Commands

Revoke License:

You can run the *tmsh* command directly or add the registration key to the *host_vars/* host file and run the Ansible playbook *revoke.yml*

```
tmsh revoke sys license
```
```
ansible-playbook -i inventory revoke.yml
```


Restore BIGIP to default:
```
tmsh load sys config default
```
